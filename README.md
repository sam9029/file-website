# testlq

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

-----------------------------------
###


Vue:卖座电影网（移动端）
全部
2021.07-2021.08
内容：
前端技术钱：vue2+axios+vant-ui+ px to rem +swiper+betterscroll+vuex
项目模块：首页轮播，正在热映，即将上映，电影详情，当地影院，城市选择，影院搜索，我的主页。
1.移动端布局适配：使用px to rem插件。
2.tabbar的显示和隐藏：有些页面需要隐藏tabbar,使用mixin混入和Vuex配合解决
3.很多页面都需要依赖于选择的城市名，ID请求数据井且相应更新，使用VUEX,井且持久化这两个数据增强用户体验。
4.2.轮播插件初始化过早问题：数据请求为异步请求，使用ne×tTick在数据返回后再进行swiper初始化解决.
5.首页电影数据加载：van-list组件配合betterScroll插件到底再次发起请求，而不是一次性加载所以有电影数据。
6.请求数据时弹出loading框，使用axios请求和响应拦截器配合vant组件库的Toast组件解决.
7.跨域问题：devServer反向代理，浏览器=&gt;服务器变为最务器=&gt;服务器没有跨域限制
8.路由优化：Vue路由懒加载，异步组件写法，根据页面按需加载组件。
9.已在gitee发布：https:/gitee.com/woodcutter-plum/wue-maizuo-cinema-system


### Question

- 电影院展示 影片海报的点击后 影片跳转

### Bug

- 影院电影排场页面
  - 点击影院进入详情 有些时候 首部电影的首日期没有 主动获取排场数据
  - 点击影院进入详情 有些时候 首部电影获取排场数据后 页面滚动到底部了
  - 还未解决 排场的日期标签 粘性定位的问题
  


- （解决）从影院电影海报 进入 电影详情页 tabbar 突然出现
  - （应该是 调转页面 vuex 的 isTabberShow 只改了---- mixin 混入调用了 挂在后 和 销毁的 钩子 （处理isTabberShow在非tabbar 页面的值变化）） 
  -  在非tabbar mounted 调用 this.$store.commit('hide')

- （解决）从没有 tabbar 的页面 直接离开程序后，在进入首页，tabbar会消失
  - 使用了 vuex持久化 控制tabbar 显示的变量isTabbarShow在非tabbar页面离开未更新
  - 在 所有 tabbar 页面 mounted 中 加入 this.$store.commit('show')


 
###
``` 文件目录


- src
  - components
    - tabbar.vue 底部导航
    - films 
    - detail 单个电影介绍页


- .browerslistrc
- .editorconfig
- .eslintrc.js
- babel.config.js
- 

```



-----------------------------------
-----------------------------------

## store index 的 持久化插件 `vuex-persistedstate` reducer配置


## Vue-filter 使用

~~~html

<van-cell v-for="data in datalist" :key="data.filmId"  @click="handleChangePage(data.filmId)">
    <div class="content">
      <div>{{data.nation}}|{{data.runtime}}分钟</div>

      <!-- data.actors (应该是传参) | actorsFilter （vue-filter） -->
      <div class="actors">主演：{{data.actors|actorsFilter}}</div>

    </div>
    </div>
</van-cell>


<script>

Vue.filter('actorsFilter', (data) => {
  if (data === undefined) return '暂无主演'
  return (data.map(item => item.name)).join('')
})

export default {
  data () {
    return {}
  }
}

</script>
~~~

